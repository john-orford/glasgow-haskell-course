

module Main where

import System.Random (randomR, getStdRandom)
import System.IO  
import Data.List

check :: String -> String -> Char -> (Bool,String)
check word display c = (
  c `elem` word, [
    if x==c
      then c
      else y | (x,y) <- zip word display
    ]
  )

turn :: String -> String -> Int -> IO ()
turn word display n
  | n==0 = putStrLn "You lose"
  | word==display = putStrLn "You win!"
  | otherwise = mkguess word display n

mkguess :: String -> String -> Int -> IO ()
mkguess word display n =
  do
    putStrLn $ display ++ "  " ++ replicate n '*'
    putStr "  Enter your guess: "
    q <- getLine
    let (correct, display') = check word display $ head q
    let n' = if correct then n else n-1
    turn word display' n'

randomLineNumber :: IO Int
randomLineNumber = getStdRandom (randomR (0,370100))

starman :: String -> Int -> IO ()
starman word = turn word ['-' | x <- word]


-- main = starman "jkdfsjh" 5

main = do  
  handle <- openFile "words_alpha.txt" ReadMode  
  contents <- hGetContents handle
  let words = lines contents
  n <- randomLineNumber
  let word = words !! n
  let w = take (length word - 1) word
  starman w 5
  hClose handle  

